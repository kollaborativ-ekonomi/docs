DocVer: 0.0.1 - ***Not approved***

# Software Requirements Specification for Smarta Kartan

This document is based on a template with this copyright: "Copyright © 1999 by Karl E. Wiegers. Permission is granted to use, modify, and distribute this document." https://cs.gmu.edu/~dfleck/classes/cs421/spring08/srs_template.doc

Revision History: See git commit notes

## 1. Introduction

#### 1. Purpose 
<!--Identify the product whose software requirements are specified in this document, including the revision or release number. Describe the scope of the product that is covered by this SRS, particularly if this SRS describes only part of the system or a single subsystem.-->

Software requirement specification (SRS) for Smarta Kartan, version 4

#### 2. Document Conventions
<!--Describe any standards or typographical conventions that were followed when writing this SRS, such as fonts or highlighting that have special significance. For example, state whether priorities  for higher-level requirements are assumed to be inherited by detailed requirements, or whether every requirement statement is to have its own priority.-->

* Image examples in this document may be from earlier versions of Smarta Kartan
* Things that are to be done are highlighted with "TODO"
* This document uses markdown, more specifically github-flavored markdown. For help on how to use markdown please see [this](https://www.markdownguide.org/)

#### 3. Intended Audience and Reading Suggestions
<!--Describe the different types of reader that the document is intended for, such as developers, project managers, marketing staff, users, testers, and documentation writers. Describe what the rest of this SRS contains and how it is organized. Suggest a sequence for reading the document, beginning with the overview sections and proceeding through the sections that are most pertinent to each reader type.-->

Jonathan, Johan, Tord and other people interested. Generally technical knowledge is not required to read this document

#### 4. Product Scope
<!--Provide a short description of the software being specified and its purpose, including relevant benefits, objectives, and goals. Relate the software to corporate goals or business strategies. If a separate vision and scope document is available, refer to it rather than duplicating its contents here.-->

The aim of the smart map is to
* provide knowledge and benefits for people who want to live a sustainable lifestyle with less linear consumption
* enable sharing initiatives and normalize circular relationships
* inspire and curate the unknown sustainable city

Read more: [On the website](https://www.smartakartan.se/om-smarta-kartan/)

#### 5. References
<!--List any other documents or Web addresses to which this SRS refers. These may include user interface style guides, contracts, standards, system requirements specifications, use case documents, or a vision and scope document. Provide enough information so that the reader could access a copy of each reference, including title, author, version number, date, and source or location.-->

Bootstrap style guide: _____

Selection of content: https://www.smartakartan.se/kriterier/


## 2. Overall Description

#### 1. Product Perspective
<!--Describe the context and origin of the product being specified in this SRS. For example, state whether this product is a follow-on member of a product family, a replacement for certain existing systems, or a new, self-contained product. If the SRS defines a component of a larger system, relate the requirements of the larger system to the functionality of this software and identify interfaces between the two. A simple diagram that shows the major components of the overall system, subsystem interconnections, and external interfaces can be helpful.-->

TODO

Version 4 of the website follows version 3, which is now live here: https://www.smartakartan.se/

The rec-spec for version 3 is available here: https://docs.google.com/document/d/1MerETncgN8kq5oeXADo5M_3h4R3SN-02BW9_AoC-X7c

#### 2. Product Functions
<!--Summarize the major functions the product must perform or must let the user perform. Details will be provided in Section 3, so only a high level summary (such as a bullet list) is needed here. Organize the functions to make them understandable to any reader of the SRS. A picture of the major groups of related requirements and how they relate, such as a top level data flow diagram or object class diagram, is often effective.-->

* User has access to a map with initiatives
  * Each initiative has a detail page
* User is inspired by the system (tips from the system) in finding new initiatives
* User can read all public content on the website
* Admin users can add new initiatives

#### 3. User Classes and Characteristics
<!--Identify the various user classes that you anticipate will use this product. User classes may be differentiated based on frequency of use, subset of product functions used, technical expertise, security or privilege levels, educational level, or experience. Describe the pertinent characteristics of each user class. Certain requirements may pertain only to certain user classes. Distinguish the most important user classes for this product from those who are less important to satisfy.-->

| User                      | Characteristics         | Considerations                                                                 |
|---------------------------|-------------------------|--------------------------------------------------------------------------------|
| New website visitor       | -                       | Ease of use. On-boarding. Many come to the site through google -> details_page |
| Returning website visitor | -                       |                                                                                |
| Admin                     | Can add new initiatives | -                                                                              |

Possible future persona/user_class: Logged in user (maybe for forum or other types of interactions)

#### 4. Operating Environment
<!--Describe the environment in which the software will operate, including the hardware platform, operating system and versions, and any other software components or applications with which it must peacefully coexist.-->

Preferable server cost less than 500 SEK/month. Currently (for v3) we Azure, pay 2000 SEK/month for azure only

Docker+Ubuntu

#### 5. Design and Implementation Constraints
<!--Describe any items or issues that will limit the options available to the developers. These might include: corporate or regulatory policies; hardware limitations (timing requirements, memory requirements); interfaces to other applications; specific technologies, tools, and databases to be used; parallel initiatives; language requirements; communications protocols; security considerations; design conventions or programming standards (for example, if the customer’s organization will be responsible for maintaining the delivered software).-->

Open source or free/libre software when possible. Ease of use for programmers always overrides other ideological statements

The code base is open source but the content is using another license. Unless otherwise specified, the content license is CC BY-NC-SA 4.0

We want to use [free/libre software](https://www.gnu.org/philosophy/free-sw.en.html), partly for ethical reasons, and also because we want to attract new developers. (Maybe AGPLv3 is a good choice?)

#### 6. User Documentation
<!--List the user documentation components (such as user manuals, on-line help, and tutorials) that will be delivered along with the software. Identify any known user documentation delivery formats or standards.-->

Website visitor: the interface is expected to be intuitive enough to not require a manual

Administrators: User manual (For v3 there is a document)

#### 7. Assumptions and Dependencies
<!--List any assumed factors (as opposed to known facts) that could affect the requirements stated in the SRS. These could include third-party or commercial components that you plan to use, issues around the development or operating environment, or constraints. The project could be affected if these assumptions are incorrect, are not shared, or change. Also identify any dependencies the project has on external factors, such as software components that you intend to reuse from another project, unless they are already documented elsewhere (for example, in the vision and scope document or the project plan).-->


## 3. External Interface Requirements

#### 1. User Interfaces
<!--Describe the logical characteristics of each interface between the software product and the users. This may include sample screen images, any GUI standards or product family style guides that are to be followed, screen layout constraints, standard buttons and functions (e.g., help) that will appear on every screen, keyboard shortcuts, error message display standards, and so on. Define the software components for which a user interface is needed. Details of the user interface design should be documented in a separate user interface specification.-->

Visitor interfaces:
* Map
* Menus
* Search
* Footer menu
* Guides for adding a new initiatives
* Details page

Admin interfaces:
* Add initiatives
* Add pages
* Add addresses

#### 2. Hardware Interfaces
<!--Describe the logical and physical characteristics of each interface between the software product and the hardware components of the system. This may include the supported device types, the nature of the data and control interactions between the software and the hardware, and communication protocols to be used.-->

We want the software to work on a wide variety of platforms, but mobile phone platforms are especially important. Examples of platforms we want to support:
* Firefox on mobile (Android/iOS)
* Chrome on Android
* Firefox on desktop (Gnu+Lin/Mac/Win)
* Chrome on desktop

#### 3. Software Interfaces
<!--Describe the connections between this product and other specific software components (name and version), including databases, operating systems, tools, libraries, and integrated commercial components. Identify the data items or messages coming into the system and going out and describe the purpose of each. Describe the services needed and the nature of communications. Refer to documents that describe detailed application programming interface protocols. Identify data that will be shared across software components. If the data sharing mechanism must be implemented in a specific way (for example, use of a global data area in a multitasking operating system), specify this as an implementation constraint.-->

![tech-overview](_img/tech-overview.jpg)

#### 4. Communications Interfaces
<!--Describe the requirements associated with any communications functions required by this product, including e-mail, web browser, network server communications protocols, electronic forms, and so on. Define any pertinent message formatting. Identify any communication standards that will be used, such as FTP or HTTP. Specify any communication security or encryption issues, data transfer rates, and synchronization mechanisms.-->

We provide a REST API so that other applications are able to read our data


## 4. System Features
<!--This template illustrates organizing the functional requirements for the product by system features, the major services provided by the product. You may prefer to organize this section by use case, mode of operation, user class, object class, functional hierarchy, or combinations of these, whatever makes the most logical sense for your product.-->

<!--
Examples here: https://github.com/EmpathyApp/EmpathyApp/blob/master/docs/planning/requirements-spec.md#4-system-features

User perspective:
* Visitor filtering by tags to explore locations
* Visitor searching to explore locations
* Visitor scrolling/browsing to explore locations
* Visitor using the map to explore locations
* Visitor filtering by tags to find already known location
* Visitor searching to find already known location
* Visitor scrolling/browsing to find already known location
* Visitor using the map to find already known location
* Visitor contacting an organizer to become a volunteer
* Admin adding new initiatives and locations

-->

#### Region-specific elements

Main page:
* Region selection
* Custom footer
* Custom links in the upper left

Other:
* contact pages
* about pages
* calendars --- TODO: This came up during a discussion

#### Main page

Link to start initiative (sv: "starta verksamhet")

#### Main page: Tags and search

#### Main page: Map

#### Main page: Cards

Cards can be sorted on:
* closest to the user
* latest update
* random
* a-z
* z-a

#### Calendar - NEW



#### Initiative details page

Description
Link to website
Social media links
Email address (important since this is a/the way that the user can interact and suggest changes)

#### Initiative details page: Map

A smaller version of the map, showing only the locations for this initiative

#### Initiative details page: Cards (new)


#### Other pages

Start initiative

#### Admin interface

for adding new initiatives and locations

Link to documentation?

#### Public API (new?)

Read-only
(Also used internally)



#### Other feature (kept here for reference)
<!--State the feature name in just a few words.-->


##### 1. Description and Priority
<!--Provide a short description of the feature and indicate whether it is of High, Medium, or Low priority. You could also include specific priority component ratings, such as benefit, penalty, cost, and risk (each rated on a relative scale from a low of 1 to a high of 9).-->


##### 2. Stimulus/Response Sequences
<!--List the sequences of user actions and system responses that stimulate the behavior defined for this feature. These will correspond to the dialog elements associated with use cases.-->

Stimulus: 

Response: 

##### 3. Functional Requirements
<!--Itemize the detailed functional requirements associated with this feature. These are the software capabilities that must be present in order for the user to carry out the services provided by the feature, or to execute the use case. Include how the product should respond to anticipated error conditions or invalid inputs. Requirements should be concise, complete, unambiguous, verifiable, and necessary. Use “TBD” as a placeholder to indicate when necessary information is not yet available.-->

* **REC-x:** System does _______
* **REC-x:** System ______
* **REC-x:** System ______

<Each requirement should be uniquely identified with a sequence number or a meaningful tag of some kind.>


## 5. Other Nonfunctional Requirements

#### 5.1 Performance Requirements
<!--If there are performance requirements for the product under various circumstances, state them here and explain their rationale, to help the developers understand the intent and make suitable design choices. Specify the timing relationships for real time systems. Make such requirements as specific as possible. You may need to state performance requirements for individual functional requirements or features.-->

As of writing this (oct 2022) we now have about 9000 visitors per month

Load speed: Green on google speed test - https://pagespeed.web.dev/

#### 5.2 Safety Requirements
<!--Specify those requirements that are concerned with possible loss, damage, or harm that could result from the use of the product. Define any safeguards or actions that must be taken, as well as actions that must be prevented. Refer to any external policies or regulations that state safety issues that affect the product’s design or use. Define any safety certifications that must be satisfied.-->

#### 5.3 Security Requirements
<!--Specify any requirements regarding security or privacy issues surrounding use of the product or protection of the data used or created by the product. Define any user identity authentication requirements. Refer to any external policies or regulations containing security issues that affect the product. Define any security or privacy certifications that must be satisfied.-->

TODO

<!--
Privacy:
* We read the user location
  * Visitor location stored?
* Google analytics?

Security:
* : Do we have sensitive data stored?

-->

#### 5.4 Software Quality Attributes
<!--Specify any additional quality characteristics for the product that will be important to either the customers or the developers. Some to consider are: adaptability, availability, correctness, flexibility, interoperability, maintainability, portability, reliability, reusability, robustness, testability, and usability. Write these to be specific, quantitative, and verifiable when possible. At the least, clarify the relative preferences for various attributes, such as ease of use over ease of learning.-->

User groups to consider:
* Developers
* Admins
* Website visitors

Maintainability: Easy to maintain the website

Usability: Website visitors can use the site without a manual or guide

Interoperability: API

Flexibility: We probably want to extend the functionality in the future

#### 5.5 Business Rules
<!--List any operating principles about the product, such as which individuals or roles can perform which functions under specific circumstances. These are not functional requirements in themselves, but they may imply certain functional requirements to enforce the rules.-->

| Person   | Roles           |
|----------|-----------------|
| Jonathan | Product owner?  |
| Johan    | Lead developer? |
| Tord     | Core developer  |
| -        | Volunteer dev   |
| -        | Admin           |

TODO: Do we want to add people from the regions here? (Part of the financing comes from seven different regions in Sweden)

#### 6. Other Requirements
<!--Define any other requirements not covered elsewhere in the SRS. This might include database requirements, internationalization requirements, legal requirements, reuse objectives for the project, and so on. Add any new sections that are pertinent to the project.-->

Internationalization: Multi-language (Swedish, English)

Reusability and adaptability: We want other people to be able to use the website for their own countries/regions, outside of Sweden

WCAG: Web Content Accessibility Guidelines - Requirement from license owners (Swedish regions: Malmö, Göteborg, etc, as of 2022)

Legal?


### Appendix A: Glossary
<!--Define all the terms necessary to properly interpret the SRS, including acronyms and abbreviations. You may wish to build a separate glossary that spans multiple projects or the entire organization, and just include terms specific to a single project in each SRS.-->

| English    | Swedish    | Description                                                     |
|------------|------------|-----------------------------------------------------------------|
| Initiative | Verksamhet | Can have zero (if digital), one (most common) or more locations |
| Location   | Plats      | A point on the map and an address                               |

Abbreviations:
* SRS: Software Requirements Specification
* v4, v3: Version 4 or 3 of the website

### Appendix B: Analysis Models
<!--Optionally, include any pertinent analysis models, such as data flow diagrams, class diagrams, state-transition diagrams, or entity-relationship diagrams.-->

### Appendix C: To Be Determined List
<!--Collect a numbered list of the TBD (to be determined) references that remain in the SRS so they can be tracked to closure.-->

2nd hand stores are by default not shown on the map or in the cards

Pay for startup and yearly license
